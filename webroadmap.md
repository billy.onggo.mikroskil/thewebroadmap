# The Very Dasar of Web Sites

## HTML (Hypertext Markup Language)
Kenali cara penulisan dokumen HTML.

Pada dokumen html kita menyebutkan peruntukan/fungsi setiap bagian halaman. Dan kita kelompokkan mereka sebagai **elemen***. Dan setiap elemen bisa memiliki attribute.

* \<head> dan \<body>
* \<p>
* \<h1>, \<h2>, \<h3>, dst
* \<a>
* \<div>
* \<image>, dsb
* \<ul>, \<li>
* \<button>
* \<input>
* ...

> link:
>
> https://www.w3schools.com/html/default.asp

## CSS (Cascading Style Sheet)
CSS memungkinan kita memberikan instruksi khusus mengenai cara menampilkan dan menempatkan element. CSS sendiri dituliskan dan ditambahkan ke dalam HTML.

> link:
>
> https://www.w3schools.com/css/default.asp

## Hosting

![hosting with nginx](./hosting_with_nginx.jpg)

Bagaimana kita menyajikan hasil kerja kita tersedia secara online kepada user (public/private).

* Pada komputer, tersedia aplikasi web server, misalnya:
  * Apache Web Server
  * Nginx
  * ...
* Copykan file-file situs kita ke dalam folder yang telah ditentukan oleh web server yang diinstall.
* Pastikan web server berada dalam jaringan yang bisa diakes oleh user Anda.

### 1. Webserver dalam jaringan lokal kita.
* Bila user kita hanyalah orang-orang internal, misalnya di dalam sebuah perusahaan atau kantor.
* Tidak bisa diakses dari luar jaringan tersebut, tidak tersedia di internet.

![web server lokal](./web_server_lokal.jpg)

### 2. Menggunakan webserver tersedia secara publik di internet 
  * biasanya berbayar.
  * menyiapkan/membeli nama domain dan hubungkan dengan webserver Anda

> link:
>
> https://www.dewaweb.com/blog/cara-membuat-website/
>
> https://www.niagahoster.co.id/blog/cara-membuat-website/
>
> https://www.hostinger.com/tutorials/

#### Share hosting vs VPS vs Dedicated Server
Secara umum hosting public terdapat beberapa solusi

![hosting_type](./hosting_type.jpg)

> link:
>
>https://www.bluehost.com/blog/shared-vs-vps-hosting-which-option-is-best-for-you/

Untuk share hosting, biasanya penyedia layanan juga menyediakan tool untuk mempermudah manajemen server kita.

![cpanel](./cpanel.jpg "cpanel")

![cpanel_file_manager](./cpanel_file_manager.jpg "cpanel file manager")

## HTTP
* adalah sebuah protokol komunikasi yang melibatkan dua perangkat komputer
* server dan client
  * server selalu siap menerima koneksi dari client
  * client akan melakukan koneksi ke server untuk mengambil data/resource yang dimiliki oleh server
* Ketika client menghubungi server, kita katakan client mengirimkan sebuah *request*.
* Ketika server mengirimkan balasan, kita sebut server mengirimkan sebuah *response*.
* sadari bahwa request HTTP sifatnya adalah Stateless
  * request, response, (setelah itu selesai)

![request response](./http_request_response.jpg "Request and Response")

![request](./request.png)
![response](./response.png)

> link:
>
> https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html

## Javascript
Tambahkan javascript agar dokumen HTML Anda bisa interaktif, misalnya:
* menu dropdown
  * menampilkan/menyembunyikan/memindahkan elemen html ketika terdapat interaksi dari user seperti click dan hover
* asynchronous http request
  * digunakan menampilkan/mengupdate data dari server

> link:
> 
> https://www.w3schools.com/js/
>
> https://javascript.info/

## The Very Very Gampang Way

* Wordpress
* Wix
* Weebly

> spolier alert:  Berbayar.

# What else you need to know
## Sifat konten dari halaman web

### Static Web Page
Misalnya kita cuman membuat sebuah portfolio diri atau perusahaan, konten kita sifatnya adalah *static*, dan tidak berubah atau setidaknya tidak berubah dalam waktu yang cukup lama.

### Dynamic Web Page
Di sisi lain, untuk menghadirkan konten *dinamis* seperti reservasi, forum, jual-beli, kita perlu dukungan database di belakang server kita.
* Website menampilkan data yang sesuai dengan data yang terdapat pada database pada saat itu.
* Artinya, Website akan menampilkan data yang berbeda bila data yang terdapat pada database kita rubah.
* Untuk menghasilkan situs dinamis, pada sisi server perlu memiliki logic/coding tambahan. (bukan pada dokumen html)
* Coding tersebut bisa jadi dalam bahasa PHP, ASP, JSP, Javascript, sdb
* Coding ini lah yang nantinya akan mengambil/mengolah database.

![dynamic_web_page](dynamic_web_page.jpg)

## Source Control / Version Control
Before lanjut ke knowledge berikutnya, kita perlu mengenal, dan mampu memanfaatkan salah satu dari *source control* sehingga menguntungkan developer dalam hal :
* manajemen kode
  * semua histori kode kita tercatat, artinya kita bisa mereview perubahan dari kode kita dari waktu ke waktu.
  * untuk setiap perubahan, kita bisa tuliskan catatan sehingga memudahkan kita saat review
* bila source code Anda di push ke GitHub, GitLab atau BitBucket, akan memungkinkan Anda kerja pada beberapa perangkat, misalnya di kantor dan di rumah.
* manajemen versi
* manajemen fitur aplikasi (dengan branching)
  * mengerjakan beberapa fitur secara terpisah tanpa mempengaruhi kode utama/inti.
* kolaborasi dengan user lain
  * bahkan tercatat siapa yang menuliskan kode apa dan siapa yang merubahnya.
  * memungkinkan kita menentukan privilege dari kolabolator. Misalnya kode perlu di approve oleh *maintainer* sebelum benar-benar di-merger ke dalam repository utama kita.

> links:
>
> https://www.atlassian.com/git/tutorials/what-is-version-control
>
> https://git-scm.com/doc

Source control di sini yang dimaksud adalah **git**. Git adalah standar de facto saat ini. Alternative lain adalah SVN, Mecurial, CVS.

## Typescript
Untuk project dengan source code besar. Why? Alasan utamanya: 

Javascript menganut prinsip *dynamic type*. Plus pada javasript intepreter(compilernya) tidak bisa mengecek kesalahan yang terjadi pada tipe data.

> links
>
> https://www.typescriptlang.org/docs/handbook/typescript-from-scratch.html
>
> https://www.educba.com/typescript-vs-javascript/

terdapat alternative lain, yaitu *flow*, tetapi secara umum lebih banyak penggunaan typescript.

> link
>
> https://blog.logrocket.com/typescript-vs-flow/

## Servers

Sebagai *gambaran besar*, **jaman now**, untuk semua big big company, termasuk untuk semua situs online shopping yang yang Anda kunjungi. Mereka tidak hanya mengandalkan satu server, melainkan sekumpulan server

![frontend_backend_db](./frontend_backend_db.jpg)

* Frontend Web Server
  menyediakan halaman-halaman HTML dalam bentuk aplikasi web.
  * Dalam hal *Aplikasi Web*, yang dimaksud HTML, CSS dan Javascript. Namun titik beratnya ada di javascript.
  * *Web app*, not just web page
* Backend Web Server
  * antar muka terhadap sistem autentikasi, alias login user.
  * antar muka dengan data yang terdapat pada database.
* Database Server
  * pusat data.
  * terproteksi secara akses tidak tersedia untuk publik.
  * backend server akan mengambil data dari sini.
* Server server dan server lagi :)

## REST API
*Representational State Transfer*, adalah salah satu standar komunikasi yang pertukaran data yang dijalankan diatas protokol HTTP.

* HTTP verb
  * GET, POST, PUT, DELETE, PATCH
* Status code
  * 404 Not Found, 200 OK
* format data
  * JSON

    ```
    {
      nama: 'Pikachu',****
      kekuatan: 'listrik',
      level: 20
    }
    ```
  * XML

    ```
    <nama>Pikachu</nama>
    <kekuatan>Pikachu</kekuatan>
    <level>20</level>
    ```
  * ...

## Development to Production
Ketahui bahwa pada saat development, kita mungkin menggunakan tool dan software yang berbeda dengan saat hasil kerja kita *deploy* ke production. Setidaknya kita harus menguasai bagaimana cara mempermudah dan mengefisiensikan proses development kita dengan perkakas yang tepat.

# Developer: What skill yang harus saya punya?

## Frontend Developer
#### Skillset yang dibutuhkan:
* **Javascript untuk manipulasi DOM/element pada html**
* **Pengetahuan CSS secara dalam**
* Pilihan Bahasa
  * Javascript (at least)
  * Typscript
  * Coffeescript
  * Dart
  * ...
* Webpack
  * webpack memungkinkan kita menuliskan javascript dalam bentuk yang lebih modular.
  * memudahkan kita dalam membundle javascript kita sebagai sebuah *aplikasi web*
* Javascript Framework
  * Angular
  * React
    * redux
  * Vue
    * vuex
  * ...
* CSS Framework
  * Bootstrap
  * Foundation
  * Bulma
  * Tailwind
  * ...
* CSS Preprosessor
  * Sass/Scss
  * Stylus
* Responsive Web Design
  * Bagaimana situs kita bisa ditampilkan dengan baik pada layar besar maupun layar kecil.
  
* Linter (ESLint atau Prettier)
  * tulis kode dalam format yang sama.
    ```
    function pangkatDua(x){
      return x*x
    }

    function pangkatTiga (x) {
      return x * x * x;
    }
    ```
* Local Browser Storage
  * LocalStorage, cookie, indexedDb

> **Cross-browser compatibility**
>
> Sadari bahwa kode kita *mungkin* tidak akan berfungsi pada setiap browser.

#### XMLHttpRequest
Sebuah http request yang dikirimkan secara programatic untuk mengambil/mengirim data dari/ke backend server dikenal juga XMLHttpRequest, atau dengan nama lain AJAX.

> Programmatic: dituliskan oleh pada kode kita, misalnya pada kode menyatakn: ketika klik tombol ini, maka update data dari server.

#### Single Page Application (Website jaman Now)

* Halaman web tidak melakukan refesh/loading halaman secara keseluruhan ketika hendak menampilkan data lain.
* Hanya satu halaman yang diloading pada awal kita visit situs tersebut.
* Selebihnya adalah manipulasi elemen-elemen DOM yang terdapat dalam dokumen html tersebut.

> Kenali kelebihan dan kekurangan SPA, kapan menggunakan konsep SPA dan kapan tidak.

#### Skill Tambahan untuk frontend developer
* Optimization
  * Minify/Uglify
  * Lazy Loading
  * Tree shaking
  * ...
* Server side rendering
* Progressive Web App
* Testing
  * Mocha, Chai, Sinon, Cypress, Jest, Selenium)
* Desktop Application (Electron)

## Backend Developer

#### Skill yang dibutuhkan:
* Pilihan Bahasa:
  * Javascript
    * menggunakan NodeJS dengan modul http server yang telah disediakan
    * menggunakan NodeJS dengan framework Express, atau HAPI, dsb
  * Typescript
  * PHP
    * menggunakan server merek Apache Web Server biasa, dengan engine PHP
  * ASP
    * produk dari Microsoft
  * JSP, Java/Servet
    * menggunakan server merek Apache Tomcat
  * Python
  * Ruby
  * Go
    * dengan modul http server yang telah disediakan.
    * dengan framework Gin Gonic, atau Echo, atau Martini, dsb
* Dasar networking
  * IP
  * PORT
  * Name resolving
* Koneksi ke database dan cara query data pada database.
  * Tergantung pada merek database yang dipakai, but setidaknya mengenal query SQL
  * Untuk database lain yang sifatnya bukan sql (alias nosql), yang paling banyak dipakai adalah MongoDB
* Indexing pada database.
* Dasar kriptografi, setidaknya mengerti konsep dasar enkripsi, hashing.

#### Advance learning for Backend Developer
* OpenAPI
  * Mendokumentasikan API
* Caching antara server pada browser
* Testing
  * Mocha, Chain, Sinon, Jest, Cypress, Selenium
* Containerization (Docker)
* OAuth
* SSO (Single Sign On)
* Web Security
  * CORS
  * HTTPS
* Continuos Integration / Continuous Delivery
* Microservices
* Message Broker
* Load Balancing

## Fullstack developer
Istilah ini dipakai untuk orang yang bekerja sebagai Backend sekaligus Frontend.

## DevOps
DevOps is a software development strategy which bridges the gap between the developers and the IT staff.

#### DevOps Engineer?
They are either developers who get interested in deployment and network operations or sysadmins who have a passion for scripting and coding and move into the development side where they can improve the planning of test and deployment

> link:
>
> https://medium.com/edureka/devops-engineer-role-481567822e06

# Roadmap
> link:
>
> https://roadmap.sh/







Catatan:
- public ip untuk dedicated server
- gambar untuk menjelaskan REST API